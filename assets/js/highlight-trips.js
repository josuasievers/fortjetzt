var windowHeight, elementPosition;

$(document).ready(function(){
	$(window).scroll(headerWatch);

	headerPosition = $('.header').offset().top;
	console.log(headerPosition);

	$('.login').click(function() {
  		$('.login-screen').toggleClass('closed');
	});


	$(window).scroll(function(){
	  var fixed = $(".to-top-fixed");

	  var fixed_position = $(".to-top-fixed").offset().top;
	  var fixed_height = $(".to-top-fixed").height();

	  var toCross_position_sport = $("#sport").offset().top;
	  var toCross_height_sport = $("#sport").height();

	  if (fixed_position + fixed_height  < toCross_position_sport) {
	    fixed.removeClass('sport-color');
	} else if (fixed_position > toCross_position_sport + toCross_height_sport) {
	    fixed.removeClass('sport-color');
	  } else {
	    fixed.addClass('sport-color');
	  }

	  var toCross_position_musik = $("#musik").offset().top;
	  var toCross_height_musik = $("#musik").height();

	  if (fixed_position + fixed_height  < toCross_position_musik) {
	    fixed.removeClass('musik-color');
	} else if (fixed_position > toCross_position_musik + toCross_height_musik) {
	    fixed.removeClass('musik-color');
	  } else {
	    fixed.addClass('musik-color');
	  }

	  var toCross_position_kunst = $("#kunst").offset().top;
	  var toCross_height_kunst = $("#kunst").height();

	  if (fixed_position + fixed_height  < toCross_position_kunst) {
	    fixed.removeClass('kunst-color');
	} else if (fixed_position > toCross_position_kunst + toCross_height_kunst) {
	    fixed.removeClass('kunst-color');
	  } else {
	    fixed.addClass('kunst-color');
	  }

	  var toCross_position_regional = $("#regional").offset().top;
	  var toCross_height_regional = $("#regional").height();

	  if (fixed_position + fixed_height  < toCross_position_regional) {
	    fixed.removeClass('regional-color');
	} else if (fixed_position > toCross_position_regional + toCross_height_regional) {
	    fixed.removeClass('regional-color');
	  } else {
	    fixed.addClass('regional-color');
	  }


	  var categories_height = $('.categories').height() + $('.categories').offset().top + 50;
	  if(fixed_position + fixed_height > categories_height)
	  {
		  $('.to-top-fixed').addClass('visible');
	  }else {
		  $('.to-top-fixed').removeClass('visible');
	  }

});


});

$('.burger').click(function() {

	$('.burger').toggleClass('open');
	if($('.burger').hasClass('open'))
	{
		$('.mobile-menu').addClass('open');
		$('.to-top-fixed').removeClass('visible');
	}
	else
	{
		$('.mobile-menu').removeClass('open');
	}
});



function headerWatch()
{
	var scrollTop = $(window).scrollTop();
	if(scrollTop >= 60){
		$('.header').addClass('slim');
		$('.logo').attr("src", "assets/img/icon.svg");
	}
	else{
		$('.header').removeClass('slim');
		$('.logo').attr("src", "assets/img/logo_01.svg");
	}
}



$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
      &&
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });
