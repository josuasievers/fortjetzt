var windowHeight, elementPosition, windowWidth, headerPosition;

$(document).ready(function(){
	$(window).scroll(headerWatch);

	headerPosition = $('.header').offset().top;

	planePosition = $('#plane').offset().top;
	elementPosition = $('#norway').offset().top;
	windowHeight = $(window).height();
	$(window).resize(function(){
		elementPosition = $('#norway').offset().top;
		if($(window).width() > 400){
			windowHeight = $(window).height();
		}

	});


	window.requestAnimationFrame(scrollWatch);

	$('.login').click(function() {
  		$('.login-screen').toggleClass('closed');
	});

	$('.burger').click(function() {
  		$('.mobile-menu').toggleClass('open');
		$('.burger').toggleClass('open');
	});
});

function scrollWatch()
{
	var scrollTop = $(window).scrollTop();
	var threshold = scrollTop + (windowHeight / 2);
	var offset = (threshold - elementPosition) * .28;
	$('#norway').css('transform', 'translate3d(-50%, ' + offset + 'px, 0)');

	offset = (threshold - planePosition) * -.3;
	$('#plane').css('transform', 'translate3d(' + (-offset * 1.5) + 'px, ' + offset  + 'px, 0)');
	

	window.requestAnimationFrame(scrollWatch);
}

function headerWatch()
{
	var scrollTop = $(window).scrollTop();
	if(scrollTop >= 60){
		$('.header').addClass('slim');
		$('.logo').attr("src", "assets/img/icon.svg");
	}
	else{
		$('.header').removeClass('slim');
		$('.logo').attr("src", "assets/img/logo_01.svg");
	}
}
